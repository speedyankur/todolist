import { Todo } from "@/store/todo.model";
import Api from "./Api";

const END_POINT = "todos";

export default {
  all() {
    return Api.get(END_POINT);
  },

  store(title: string) {
    const todo = {
      title,
    };
    return Api.post(END_POINT, todo);
  },
  delete(id: string) {
    return Api.delete(`${END_POINT}?id=${id}`);
  },
  update(_todo: Todo, title: string) {
    const todo: Todo = JSON.parse(JSON.stringify(_todo));
    todo.title = title;
    console.log(todo);
    return Api.put(END_POINT, todo);
  },
  toggle(_todo: Todo) {
    const todo: Todo = JSON.parse(JSON.stringify(_todo));
    todo.completed = !todo.completed;
    return Api.put(END_POINT, todo);
  },
  deleteAll() {
    return Api.delete(END_POINT);
  },
};

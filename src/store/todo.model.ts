export interface Todo {
  id: string;
  title: string;
  completed: boolean;
}

export interface TodoState {
  todos: Array<Todo>;
}

export interface TODOUpdateInput {
  title?: string;
  completed?: boolean;
  todo: Todo;
}

export enum FILTER {
  ALL,
  ACTIVE,
  COMPLETE,
}

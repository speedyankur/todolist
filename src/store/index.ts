import Vue from "vue";
import Vuex from "vuex";
import Todos from "@/apis/Todos";
import { Todo, TodoState, TODOUpdateInput } from "./todo.model";
Vue.use(Vuex);

const state: TodoState = {
  todos: [],
};

const getters = {
  todos: (state: TodoState) => state.todos,
  leftItems: (state: TodoState) => {
    console.log("checking leftItems");
    return state.todos.filter((todo) => !todo.completed);
  },
};

const actions = {
  async fetchTodos(context: any) {
    const response = await Todos.all();
    context.commit("SET_TODOS", response.data);
  },
  async addTodo(context: any, title: string) {
    const response = await Todos.store(title);
    if (response.status === 200) {
      const todo: Todo = {
        id: response.data[0].id,
        title: response.data[0].fields.title,
        completed: false,
      };
      context.commit("ADD_TODO", todo);
    }
  },
  async toggleTodo(context: any, todo: Todo) {
    const response = await Todos.toggle(todo);
    if (response.status === 200) {
      todo.completed = !todo.completed;
      context.commit("UPDATE_TODO", todo);
    }
  },
  async updateTodo(context: any, data: { todo: Todo; title: string }) {
    const response = await Todos.update(data.todo, data.title);
    if (response.status === 200) {
      data.todo.title = data.title;
      context.commit("UPDATE_TODO", data.todo);
    }
  },
  async deleteTodo(context: any, todo: Todo) {
    const response = await Todos.delete(todo.id);
    if (response.status === 200) {
      context.commit("DELETE_TODO", todo);
    }
  },
};

const mutations = {
  SET_TODOS(s: TodoState, data: Array<Todo>) {
    s.todos = data;
  },
  ADD_TODO(s: TodoState, todo: Todo) {
    s.todos.push(todo);
  },
  DELETE_TODO(s: TodoState, todo: Todo) {
    const index = s.todos.findIndex((t) => t.id === todo.id);
    s.todos.splice(index, 1);
  },
  UPDATE_TODO(s: TodoState, todo: Todo) {
    const index = s.todos.findIndex((t) => t.id === todo.id);
    Vue.set(s.todos, index, todo);
  },
};

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
});

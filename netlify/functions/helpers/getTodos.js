const { table } = require("./airtable");
const formattedReturn = require("./formattedReturn");
module.exports = async event => {
  try {
    const options = {
      fields: ["completed", "title"]
    };
    try {
      if (event.queryStringParameters.title) {
        options.filterByFormula = `SEARCH("${event.queryStringParameters.title}", {title})`;
      }
    } catch (err) {
      console.error(err);
    }
    const todos = await table.select(options).firstPage();
    const formattedTodos = todos.map(todo => ({
      id: todo.id,
      ...todo.fields
    }));
    return formattedReturn(200, formattedTodos);
  } catch (err) {
    console.error(err);
    return formattedReturn(500, {});
  }
};

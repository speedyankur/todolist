const { table } = require("./airtable");
const formattedReturn = require("./formattedReturn");
module.exports = async (event) => {
  const id = event.queryStringParameters.id;
  try {
    const deletedTodo = await table.destroy(id);
    return formattedReturn(200, deletedTodo);
  } catch (err) {
    console.error(err);
    return formattedReturn(500, {});
  }
};

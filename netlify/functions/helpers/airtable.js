var Airtable = require("airtable");
const { AIRTABLE_LIST, AIRTABLE_BASE, AIRTABLE_API_KEY } = process.env;
var base = new Airtable({ apiKey: AIRTABLE_API_KEY }).base(AIRTABLE_BASE);
const table = base(AIRTABLE_LIST);
module.exports = { table };

const formattedReturn = require("./helpers/formattedReturn");
const getTodos = require("./helpers/getTodos");
const createTodo = require("./helpers/createTodo");
const deleteTodo = require("./helpers/deleteTodo");
const updateTodo = require("./helpers/updateTodo");
exports.handler = async event => {
  if (event.httpMethod === "GET") {
    return await getTodos(event);
  } else if (event.httpMethod === "POST") {
    return await createTodo(event);
  } else if (event.httpMethod === "PUT") {
    return await updateTodo(event);
  } else if (event.httpMethod === "DELETE") {
    return await deleteTodo(event);
  } else {
    return formattedReturn(405, {});
  }
};
